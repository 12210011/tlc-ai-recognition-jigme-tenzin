FROM python:3.8-slim
#Use an existing linux os with python 3.8 installed

RUN apt-get update -y
#Update all the libraries in the os

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN pip install -r libraries.txt
#install all required Python packages

ADD . /app

EXPOSE 5000
#open port 5000

CMD waitress-serve --listen 0.0.0.0:$PORT app:app
#start waitress web server
